[![crates.io](https://img.shields.io/crates/v/treeculler)](https://crates.io/crates/treeculler)
[![docs.rs](https://docs.rs/treeculler/badge.svg)](https://docs.rs/treeculler)

# treeculler
Implements frustum culling. In the future, it will also implement a few occlusion culling methods, and a BVH (bounding volume hierarchy) based frustum culling.

Please check library documentation for more information.